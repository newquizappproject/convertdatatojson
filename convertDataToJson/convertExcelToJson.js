var Excel = require('exceljs');
var crypto = require('crypto-js');
var fs = require('fs');
const { base64encode, base64decode } = require('nodejs-base64');
const workbook = new Excel.Workbook();
const arrayColNum = [2, 3, 4, 5, 6, 8, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 34, 35];
var data = [];
const keyEncrypt = "qwertyuiopASDFGHJKLzxcvbnm123456789";

workbook.xlsx.readFile("NQAP_Content (Questions)_Data_v.0.1.xlsx").then(function() {
        // xử lý trong sheet [Management] lấy danh sách các sheet cần import data
        var worksheetMan = workbook.getWorksheet('Management');
        let sheetProcess = findSheetsProcess(worksheetMan);
        let rowObj = {}
        // Nếu có danh sách các sheet cần đọc dữ liệu thì xử lý tiếp
        if (sheetProcess.length > 0) {
            // check validate
            let errors = validateData(sheetProcess);
            if (typeof errors !== 'undefined' && errors.length > 0) {
                // Tạo ra file data có cấu trúc JSON, đuôi file là *.txt
                generateTextFile(JSON.stringify(errors));
                return; 
            }

            // loop danh sách các sheet cần import data
            sheetProcess.forEach(function(sheetName){
                let worksheet = workbook.getWorksheet(sheetName);
                console.log(sheetName);
                // đọc từng hàng trong sheet cần import data
                worksheet.eachRow(function(row, rowNumber) {
                    if (rowNumber > 1 && formatData(row.getCell(5).text) === '0') {
                        // import data
                        rowObj = new Object();
                        for(let i = 0; i < arrayColNum.length; i++) {
                            switch(arrayColNum[i]) {
                                case 2:
                                    rowObj.ID = formatData(row.getCell(arrayColNum[i]).text);
                                    break;
                                case 3:
                                    rowObj.ExamProvider = formatData(row.getCell(arrayColNum[i]).text);
                                    break;
                                case 4:
                                    rowObj.ExamID = formatData(row.getCell(arrayColNum[i]).text);
                                    break;
                                case 5:
                                    rowObj.IsDeleted = formatData(row.getCell(arrayColNum[i]).text);    
                                    break;
                                case 6:
                                    rowObj.QuestionType = formatData(row.getCell(arrayColNum[i]).text);
                                    break;
                                case 8:
                                    rowObj.QuestionDomain = formatData(row.getCell(arrayColNum[i]).text);
                                    break;
                                case 11:
                                    rowObj.QuestionContent = crypto.AES.encrypt(formatData(row.getCell(arrayColNum[i]).text), keyEncrypt).toString();
                                    break;
                                case 12:
                                    rowObj.Option1 = crypto.AES.encrypt(formatData(row.getCell(arrayColNum[i]).text), keyEncrypt).toString();
                                    break;
                                case 13:
                                    rowObj.Option2 = crypto.AES.encrypt(formatData(row.getCell(arrayColNum[i]).text), keyEncrypt).toString();
                                    break;
                                case 14:
                                    rowObj.Option3 = crypto.AES.encrypt(formatData(row.getCell(arrayColNum[i]).text), keyEncrypt).toString();
                                    break;
                                case 15:
                                    rowObj.Option4 = crypto.AES.encrypt(formatData(row.getCell(arrayColNum[i]).text), keyEncrypt).toString();
                                    break;
                                case 16:
                                    var option5Templ = formatData(row.getCell(arrayColNum[i]).text);
                                    if (option5Templ != '') {
                                        rowObj.Option5 = crypto.AES.encrypt(option5Templ, keyEncrypt).toString();
                                    }
                                    else {
                                        rowObj.Option5 = "";
                                    }
                                    break;
                                case 17:
                                    var option6Templ = formatData(row.getCell(arrayColNum[i]).text);
                                    if (option6Templ != '') {
                                        rowObj.Option6 = crypto.AES.encrypt(option6Templ, keyEncrypt).toString();
                                    }
                                    else {
                                        rowObj.Option6 = "";
                                    }
                                    break;
                                case 18:
                                    var option7Templ = formatData(row.getCell(arrayColNum[i]).text);
                                    if (option7Templ != '') {
                                        rowObj.Option7 = crypto.AES.encrypt(option7Templ, keyEncrypt).toString();
                                    }
                                    else {
                                        rowObj.Option7 = "";
                                    }
                                    break;
                                case 19:
                                    var option8Templ = formatData(row.getCell(arrayColNum[i]).text);
                                    if (option8Templ != '') {
                                        rowObj.Option8 = crypto.AES.encrypt(option8Templ, keyEncrypt).toString();
                                    }
                                    else {
                                        rowObj.Option8 = "";
                                    }
                                    break;
                                case 20:
                                    var option9Templ = formatData(row.getCell(arrayColNum[i]).text);
                                    if (option9Templ != '') {
                                        rowObj.Option9 = crypto.AES.encrypt(option9Templ, keyEncrypt).toString();
                                    }
                                    else {
                                        rowObj.Option9 = "";
                                    }
                                    break;
                                case 21:
                                    var option10Templ = formatData(row.getCell(arrayColNum[i]).text);
                                    if (option10Templ != '') {
                                        rowObj.Option10 = crypto.AES.encrypt(option10Templ, keyEncrypt).toString();
                                    }
                                    else {
                                        rowObj.Option10 = "";
                                    }
                                    break;
                                case 34:
                                    rowObj.CorrectAnswer = formatData(row.getCell(arrayColNum[i]).text).toString();
                                    break;
                                case 35:
                                    rowObj.Explanation = crypto.AES.encrypt((formatData(row.getCell(arrayColNum[i]).text)), keyEncrypt).toString();
                                    break;
                                default:
                                    continue;
                            }
                        }
                        data.push(rowObj);
                    }
                });
            });
            // Tạo ra file data có cấu trúc JSON, đuôi file là *.txt
            generateTextFile(JSON.stringify(data));
        }
    }
);


/**
 * lấy danh sách các sheet cần đọc dữ liệu tại sheet [Management]
 * @param Worksheet worksheet 
 * @returns danh sách sheet cần import data
 */
function findSheetsProcess(worksheet){
    let sheetsNeedProcess = [];
    worksheet.eachRow(function(row, rowNumber) {
        if(rowNumber >= 3) {
            if(row.getCell(4).value === 'PMP' && row.getCell(7).value === 'DONE'
                && row.getCell(8).value === 'DONE' && row.getCell(9).value === 'DONE') {
                // console.log(row.getCell(6).value);
                sheetsNeedProcess.push(row.getCell(6).value);
            }
        }
    });
    return sheetsNeedProcess;
}

function validateData(sheetProcess) {
    let errors = [];
    let idArr = [];
    // loop danh sách các sheet cần import data
    sheetProcess.forEach(function(sheetName){
        let worksheet = workbook.getWorksheet(sheetName);
        // đọc từng hàng trong sheet cần import data
        worksheet.eachRow(function(row, rowNumber) {
            if (rowNumber > 1) {
                // ID * (Kiểm tra đã nhập chưa)
                let id = row.getCell(2).text.trim();
                if(id === null || id === '') {
                    errors.push('ID tại câu số ' + id + ' tại sheet ' + sheetName + ' chưa được nhập ');
                }
                // ExamProvider * (Kiểm tra đã nhập chưa)
                let examProvider = row.getCell(3).text.trim();
                if(examProvider === null || examProvider === '') {
                    errors.push('ExamProvider tại câu số ' + id + ' tại sheet ' + sheetName + ' chưa được nhập ');
                }

                // ExamID * (Kiểm tra đã nhập chưa)
                let examID = row.getCell(4).text.trim();
                if(examID === null || examID === '') {
                    errors.push('ExamID tại câu số ' + id + ' tại sheet ' + sheetName + ' chưa được nhập ');
                }

                // QuestionType * (Kiểm tra đã nhập chưa)
                let questionType = row.getCell(6).text.trim();
                if(questionType === null || questionType === '') {
                    errors.push('QuestionType tại câu số ' + id + ' tại sheet ' + sheetName + ' chưa được nhập ');
                }

                // QuestionDomain * (Kiểm tra đã nhập chưa)
                let questionDomain = row.getCell(8).text.trim();
                if(questionDomain === null || questionDomain === '') {
                    errors.push('QuestionDomain tại câu số ' + id + ' tại sheet ' + sheetName + ' chưa được nhập ');
                }

                // QuestionContent * (Kiểm tra đã nhập chưa)
                let questionContent = row.getCell(11).text.trim();
                if(questionContent === null || questionContent === '') {
                    errors.push('QuestionContent tại câu số ' + id + ' tại sheet ' + sheetName + ' chưa được nhập ');
                }

                // CorrectAnswer * (Kiểm tra đã nhập chưa)
                let correctAnswer = row.getCell(34).text.trim();
                if(correctAnswer === null || correctAnswer === '') {
                    errors.push('correctAnswer tại câu số ' + id + ' tại sheet ' + sheetName + ' chưa được nhập ');
                }
                // Kiểm tra có bị trùng ID
                if(id !== null && id !== '') {
                    if (!idArr.includes(id)) {
                        idArr.push(id);
                    } else {
                        errors.push('ID số ' + id + ' tại sheet ' + sheetName);
                    }
                }

                // Kiểm tra các option có bị trùng ko
                let option1 = row.getCell(12).text.trim();
                let option2 = row.getCell(13).text.trim();
                let option3 = row.getCell(14).text.trim();
                let option4 = row.getCell(15).text.trim();
                let option5 = row.getCell(16).text.trim();
                let option6 = row.getCell(17).text.trim();
                let option7 = row.getCell(18).text.trim();
                let option8 = row.getCell(19).text.trim();
                let option9 = row.getCell(20).text.trim();
                let option10 = row.getCell(21).text.trim();
                var answerArray = [option1, option2, option3, option4, option5, option6, option7, option8, option9, option10];
                var results = [];
                for(var i = 0; i < answerArray.length; i++) {
                    if (answerArray[i] !== '') {
                        if (results.indexOf(answerArray[i]) !== -1) {
                            errors.push('option với giá trị ' + answerArray[i] +' tại câu số ' + id + ' tại sheet ' + sheetName + ' bị duplicate giá trị ');
                        } else {
                            results.push(answerArray[i]);
                        }
                    }
                }

                // Kiểm tra trường IsDeleted
                let isDeleted = row.getCell(5).text.trim();
                if (isDeleted !== '0' & isDeleted !== '1') {
                    errors.push('isDeleted tại câu số ' + id + ' tại sheet ' + sheetName + ' cần nhập giá trị là 0 hoặc 1 ');
                }

            }
        });
    });
    return errors;
}

/**
 * format lại data input trong excel
 * bỏ khoảng trắng đầu cuối
 * Chứ cái đầu thì viết hoa
 * @param object input 
 * @returns 
 */
function formatData(input) {
    const str = input.trim();
    let results = "";
    if(str !== null && str !== '') {
       results = str.charAt(0).toUpperCase() + str.slice(1);
    }
    return results;
}


/**
 * Tạo ra file data có cấu trúc JSON, đuôi file là *.txt
 * @param Object data 
 */
function generateTextFile (data) {
    fs.writeFileSync('quizDataInput.txt', data);
}
